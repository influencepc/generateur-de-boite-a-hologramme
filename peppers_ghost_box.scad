/* Copyright Vincent LAMBERT. Contributor Sylvain GARNAVAULT. (2016)
email: vincent@influence-pc.fr
This software is a computer program whose purpose is to generate the 2D plan of a pyramid or a prism for making a hologram based on the Pepper's Ghost effect.
This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author,  the holder of the economic rights, and the successive licensors  have only  limited liability.
In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms. */



/*** VARIABLES YOU NEED TO FILL ***/

/** Configurable parameters in millimeters **/
box_thickness = 10;
columns_thickness = 8;
prism_thickness = 6;
monitor_width = 420;     // Important, fill with precision
monitor_height = 375;
monitor_thickness = 50;   // The "depth" of the monitor.
monitor_biggest_height_border = 10; // Biggest border on top or on bottom of the screen.
screen_width = 380;        // Screen surface only,
screen_height = 305;       // not including the monitor's border.
prism_width = screen_width; // Default to screen_width. Could be equal to screen_height if you want a square based pyramid.
prism_depth = screen_height;
box_padding = 12;           // Additionnal space left between each side of the monitor and columns. Necessary to get screen fixations!
bottom_box_height = 100;
top_box_height_margin = 40; // You can add a small padding above the monitor. A typical case for Raspberry Pi is around 4cm height.
top_opening_diameter = 100;
top_side_opening_diameter = 50;
handles_height = 30;
handles_width = 70;
gamepads_holes_diameter = 20;

/** Configurable angle in degrees **/
// The angle between a prism face and the screen or the floor.
// Most tutorials use 54.7° because it creates equilateral triangles, easier to build.
// If the angle of reflection is 45°, the light will reflect at 90° of the screen (horizontally).
prism_face_angle = 45;

/** Configurable percentage of free space (not usable by the screen) on top of the prism **/
free_space_area_percentage = 5;   // In % of prism_depth. The prism top will be cut to create a hole. It reduces faces height and prism height.

/** Others parameters **/
column_width_multiple = 4; // An columns_thickness multiple. Define the columns width.
top_box_active = true;
bottom_box_active = true;
handles = true;
top_opening = true;
top_side_opening = true;
gamepads_holes = true;



/*** AUTOMATED SCRIPT  ***/

include <lasercut.scad>;

/** Automatic parameters **/

/* About the prism */
top_edge_length = prism_width - prism_depth; // Distance between the two tops (it's not really a pyramid, it's a polyhedron)
free_space_width = free_space_area_percentage * prism_width / 100;
free_space_depth = free_space_area_percentage * prism_depth / 100;
initial_prism_face_height = (prism_depth/2) / cos(prism_face_angle);   // Hypotenuse = triangle height
initial_prism_height = (prism_depth/2) * tan(prism_face_angle);  // Prism height (not the triangle height)
final_prism_height = (prism_depth/2 - free_space_depth/2) * tan(prism_face_angle);
final_prism_face_height = (prism_depth/2 - free_space_depth/2) / cos(prism_face_angle);

/* About the box */
screen_min_length = screen_width < screen_height ? screen_width : screen_height;
screen_max_length = screen_width > screen_height ? screen_width : screen_height;
monitor_max_size = monitor_width > (screen_height + monitor_biggest_height_border*2) ? monitor_width : (screen_height + monitor_biggest_height_border*2);
top_box_height = monitor_thickness + top_box_height_margin;
space_for_columns = (columns_thickness*3)*2;
length_with_columns = monitor_max_size - screen_max_length < space_for_columns ? screen_max_length + space_for_columns : monitor_max_size;
box_size = length_with_columns + box_padding*2;
holes_shift = columns_thickness*column_width_multiple/2+columns_thickness/2;
column_positions = [
    // bottom left
    [columns_thickness+holes_shift, columns_thickness, columns_thickness, columns_thickness], [columns_thickness, columns_thickness+holes_shift, columns_thickness, columns_thickness],
    // bottom right
    [box_size-columns_thickness*2-holes_shift, columns_thickness, columns_thickness, columns_thickness], [box_size-columns_thickness*2, columns_thickness+holes_shift, columns_thickness, columns_thickness],
    // top right
    [box_size-columns_thickness*2, box_size-columns_thickness*2-holes_shift, columns_thickness, columns_thickness], [box_size-columns_thickness*2-holes_shift, box_size-columns_thickness*2, columns_thickness, columns_thickness],
    // top left
    [columns_thickness+holes_shift, box_size-columns_thickness*2, columns_thickness, columns_thickness], [columns_thickness, box_size-columns_thickness*2-holes_shift, columns_thickness, columns_thickness]
];
column_width = columns_thickness * column_width_multiple;
screen_fixation_with = columns_thickness + box_padding - 3; // 3mm by security



/** Prism parts' definitions **/
module face_for_prism_width() {
    lasercutout(thickness = prism_thickness, points = [[0, 0], [prism_width, 0], [prism_width/2 + free_space_depth/2 + top_edge_length/2, final_prism_face_height], [prism_width/2 - top_edge_length/2 - free_space_depth/2, final_prism_face_height]]);
}
module face_for_prism_depth() {
    lasercutout(thickness = prism_thickness, points = [[0, 0], [prism_depth, 0], [prism_depth/2 + free_space_depth/2, final_prism_face_height], [prism_depth/2 - free_space_depth/2, final_prism_face_height]]);
}

/** Prism definitions **/
module prism() {
    translate([prism_width, 0, 0])
        rotate([0, 0, 90]) {
            rotate([prism_face_angle, 0, 0])
                face_for_prism_depth();

            translate([prism_depth, 0, 0])
                rotate([prism_face_angle, 0, 90])
                    face_for_prism_width();

            translate([0, prism_width, 0])
                rotate([-(prism_face_angle), 0, 0])
                    translate([0, 0, prism_thickness])
                        rotate([180, 0, 0])
                            face_for_prism_depth();

            translate([0, 0, 0])
                rotate([-(prism_face_angle), 0, 90])
                    translate([0, 0, prism_thickness])
                        rotate([180, 0, 0])
                            face_for_prism_width();
        }
}

/** Box parts' definitions **/
module floor_board(windows = [], finger_joints = false, simple_tab_holes = false, circles_remove = false) {
    tabs = finger_joints == false ? [] : [[UP, 1, 6], [RIGHT, 1, 6], [DOWN, 1, 6], [LEFT, 1, 6]];
    holes = simple_tab_holes == false ? [] : column_positions;
    lasercutoutSquare(thickness = box_thickness, x = box_size, y = box_size, cutouts = concat(windows, holes), finger_joints = tabs, circles_remove = circles_remove);
}
module top_board(finger_joints = false) {
    floor_board([
        [(box_size/2)-(screen_width/2), (box_size/2)-(screen_height/2), screen_width, screen_height]],  //screen surface
        finger_joints = finger_joints,
        simple_tab_holes = true
    );
}
module second_top_board(top_opening = false) {
    top_hole = top_opening == false ? [] : [[top_opening_diameter, box_size/2, box_size/2]];
    translate([box_size, 0, box_thickness])
        rotate([0, 180, 0])
            floor_board(finger_joints = true, circles_remove = top_hole);
}
module bottom_board() {
    translate([box_size, 0, box_thickness])
        rotate([0, 180, 0])
            floor_board(finger_joints = true);
}
module bottom_side_board(activate_handles = false) {
    circles = activate_handles == false ? [] : [[handles_height/2, box_size/2-handles_width/2, bottom_box_height/2], [handles_height/2, box_size/2+handles_width/2, bottom_box_height/2]];
    rectangle = activate_handles == false ? [] : [[box_size/2-handles_width/2, bottom_box_height/2 - handles_height/2, handles_width, handles_height]];
    lasercutoutSquare(thickness = box_thickness, x = box_size, y = bottom_box_height, finger_joints = [[UP, 1, 6], [RIGHT, 1, 2], [DOWN, 1, 6], [LEFT, 1, 2]], circles_remove = circles, cutouts = rectangle);
}
module top_side_board(gamepad_hole = false, top_side_opening = false) {
    hole = gamepad_hole == false ? [] : [[gamepads_holes_diameter/2, box_size-columns_thickness*2-holes_shift, top_box_height/2]];
    final_hole = top_side_opening == false ? hole : [[top_side_opening_diameter/2, box_size-columns_thickness*2-holes_shift, top_box_height/2]];
    lasercutoutSquare(thickness = box_thickness, x = box_size, y = top_box_height, finger_joints = [[UP, 1, 6], [RIGHT, 1, 2], [DOWN, 1, 6], [LEFT, 1, 2]], circles_remove = final_hole);
}
module column(reverse = false) {
    if (reverse == false) {
        normal_column();
    } else {
        reverse_column();
    }
}
module normal_column() {
    lasercutoutSquare(thickness = columns_thickness, x = column_width, y = final_prism_height,
        simple_tabs = [[DOWN, column_width/2, 0], [UP, column_width/2, final_prism_height]],
        finger_joints = [[LEFT, 1, 4]]
    );
}
module reverse_column() {
    translate([0, final_prism_height, columns_thickness])
        rotate([180, 0, 0])
            normal_column();
}
module right_column(reverse = false) {
    translate([column_width-(column_width_multiple-1)*columns_thickness/2, 0, box_thickness])
        rotate([90, 0, 180])
            column(reverse = reverse);
}
module top_column(reverse = false) {
    translate([columns_thickness, (column_width_multiple+1)*columns_thickness/2, box_thickness])
        rotate([90, 0, 270])
            column(reverse = reverse);
}
module left_column(reverse = false) {
    translate([-(column_width_multiple-1)*columns_thickness/2, columns_thickness, box_thickness])
        rotate([90, 0, 0])
            column(reverse = reverse);
}
module bottom_column(reverse = false) {
    translate([0, -(column_width_multiple-1)*columns_thickness/2, box_thickness])
        rotate([90, 0, 90])
            column(reverse = reverse);
}
module screen_fixation() {
    lasercutoutSquare(thickness = box_thickness, x = screen_fixation_with, y = monitor_height);
}

/* Box definitions */
module base_box(finger_joints_bottom = false, finger_joints_top = false) {
    // Add box bottom
    floor_board(finger_joints = finger_joints_bottom, simple_tab_holes = true);

    // Add the prism
    translate([box_size/2 - prism_width/2, box_size/2 - prism_depth/2, box_thickness])
        prism();

    // Add columns
    if (true) {
        translate([column_positions[0][1], column_positions[0][0], 0])
            bottom_column();
        translate([column_positions[1][1], column_positions[1][0], 0])
            left_column(reverse = true);
        translate([column_positions[2][0], column_positions[2][1], 0])
            right_column();
        translate([column_positions[3][0], column_positions[3][1], 0])
            bottom_column(reverse = true);
        translate([column_positions[4][0], column_positions[4][1], 0])
            top_column();
        translate([column_positions[5][0], column_positions[5][1], 0])
            right_column(reverse = true);
        translate([column_positions[6][0], column_positions[6][1], 0])
            left_column();
        translate([column_positions[7][0], column_positions[7][1], 0])
            top_column(reverse = true);
    }

    // Add box top
    translate([0, 0, box_thickness + final_prism_height])
        top_board(finger_joints = finger_joints_top);

    translate([0, box_size/2-monitor_height/2, box_thickness*2 + final_prism_height])
        screen_fixation();
    translate([monitor_height+box_size/2-monitor_height/2, 0, box_thickness*2 + final_prism_height])
        rotate([0, 0, 90])
            screen_fixation();
    translate([box_size-box_thickness-screen_fixation_with, box_size/2-monitor_height/2, box_thickness*2 + final_prism_height])
        screen_fixation();
    translate([monitor_height+box_size/2-monitor_height/2, box_size-box_thickness-screen_fixation_with, box_thickness*2 + final_prism_height])
        rotate([0, 0, 90])
            screen_fixation();
}
module bottom_box() {
    bottom_board();

    translate([0, 0, box_thickness])
        rotate([90, 0, 0])
            bottom_side_board();

    translate([box_size, 0, box_thickness])
        rotate([90, 0, 90])
            bottom_side_board(handles);

    translate([0, box_size, box_thickness + bottom_box_height])
        rotate([270, 0, 0])
            bottom_side_board();

    translate([0, 0, box_thickness + bottom_box_height])
        rotate([270, 0, 90])
            bottom_side_board(handles);
}
module top_box() {
    translate([0, 0, top_box_height])
        second_top_board(top_opening = top_opening);

    translate([0, -box_thickness, top_box_height])
        rotate([270, 0, 0])
            top_side_board(top_side_opening = top_side_opening);

    translate([box_size + box_thickness, 0, top_box_height])
        rotate([270, 0, 90])
            top_side_board(gamepad_hole = gamepads_holes);

    translate([box_size, box_size + box_thickness, top_box_height])
        rotate([90, 180, 0])
            top_side_board(gamepad_hole = gamepads_holes);

    translate([-box_thickness, box_size, top_box_height])
        rotate([90, 180, 90])
            top_side_board(gamepad_hole = gamepads_holes);
}



/*** Build the box ***/
if (true) {
    if (bottom_box_active == true && top_box_active == true) {
        bottom_box();
        translate([0, 0, box_thickness + bottom_box_height])
            base_box(finger_joints_bottom = true, finger_joints_top = true);
        translate([0, 0, box_thickness + bottom_box_height + box_thickness + final_prism_height + box_thickness])
            top_box();
    } else if (bottom_box_active == true && top_box_active == false) {
        bottom_box();
        translate([0, 0, box_thickness + bottom_box_height])
            base_box(finger_joints_bottom = true, finger_joints_top = false);
    } else if (bottom_box_active == false && top_box_active == true) {
        base_box(finger_joints_bottom = false, finger_joints_top = true);
        translate([0, 0, box_thickness + final_prism_height + box_thickness])
            top_box();
    } else {
        base_box();
    }
}

// bumpy_finger_joints
